<?php

use App\Http\Controllers\musicController;
use App\Models\music;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('playlist');
});

Route::get('playlist/agregar', [musicController::class, 'create'])->name('playlist.agregar');
Route::post('playlist/guardar', [musicController::class, 'store'])->name('playlist.store');
Route::get('playlist/listar', [musicController::class, 'index'])->name('playlist.index');
Route::get('playlist/{song}/editar', [musicController::class, 'edit'])->name('playlist.edit');
Route::put('playlist/{song}/actualizar', [musicController::class, 'update'])->name('playlist.update');
Route::get('playlist/{song}/ver', [musicController::class, 'show'])->name('playlist.show');
Route::delete('playlist/{song}/eliminar', [musicController::class, 'destroy'])->name('playlist.destroy');