<?php

namespace App\Http\Controllers;

use App\Models\music;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;

class musicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $songs= music::orderByDesc('id')->get();
        return view('playlist.index', compact('songs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('playlist.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request ->validate([
            'nombre' => 'required | max:45',
            'artista' => 'required | max:45',
            'ano' => 'required | numeric',
            'genero' => 'required | in:Blues,Rap,Rock,Trap,Pop,EDM'
        ]);
        $song = music::create($validated);
        return redirect()->route('playlist.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\music  $music
     * @return \Illuminate\Http\Response
     */
    public function show(music $song)
    {
        return view('playlist.show', ['song' => $song]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\music  $music
     * @return \Illuminate\Http\Response
     */
    public function edit(music $song)
    {
        return view('playlist.edit', compact('song'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\music  $music
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, music $song)
    {
        $datos = $request ->validate([
            'nombre' => 'required | max:45',
            'artista' => 'required | max:45',
            'ano' => 'required | numeric',
            'genero' => 'required | in:Blues,Rap,Rock,Trap,Pop,EDM'
        ]);
        $song->update($datos);
        return redirect()->route('playlist.index'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\music  $music
     * @return \Illuminate\Http\Response
     */
    public function destroy(music $song)
    {
        $song->delete();
        return redirect()->route('playlist.index');
    }
}
