<?php

namespace App\View\Components;

use App\Models\music;
use Illuminate\View\Component;

class form extends Component
{
    private $song;
    /**
     * Create a new component instance.
     * @param \App\Models\music $song
     * @return void
     */
    public function __construct($song = null)
    {
        if( is_null($song)){
            $song = music::make([]);
        }
        $this->song = $song;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $params = [
            'song' => $this->song,
        ];
        return view('components.form', $params);
    }
}
