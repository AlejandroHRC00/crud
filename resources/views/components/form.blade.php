    @csrf
    <div class="col-sm-12">
        <label for="InputNombre" class="form-label">Nombre de la canción</label>
        <input type="text" name="nombre" id="InputNombre" class="form-control" placeholder="..." value="{{ old('nombre', $song->nombre) }}">
    </div>
    <div class="col-sm-12">
        <label for="InputArtista" class="form-label">Nombre del artista</label>
        <input type="text" name="artista" id="InputArtista" class="form-control" placeholder="..." value="{{ old('artista', $song->artista) }}">
    </div>
    <div class="col-sm-10">
        <label for="InputAno" class="form-label">Año de lanzamineto</label>
        <input type="number" name="ano" id="InputAno" class="form-control" placeholder="..." value="{{ old('ano', $song->ano) }}">
        </div>
    <div class="col-sm-6">
        <label for="SelectGenero" class="form-label">Género</label>
        <select name="genero" id="selectUrgencia" class="form-select">
            <option value="Blues">Blues</option>
            <option value="Rap">Rap</option>
            <option value="Rock">Rock</option>
            <option value="Trap">Trap</option>
            <option value="Pop">Pop</option>
            <option value="EDM">EDM</option>
        </select>
   </div>
   <div class="col-sm-12 tect-end my-2">
        <button type="submit" class="btn btn-primary">Aceptar</button>
   </div>