<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
</head>
<body class="bg-light">
    <div class="container shadow bg-white my-2">
        <div class="row">
            <div class="col-sm-12">
                <h1>Mi playlist</h1>
            </div>
            <div class="col-sm-12">
                <a href="{{ route('playlist.agregar') }}" class="btn btn-link">Agregar canción</a>
                <a href="{{ route('playlist.index') }}" class="btn btn-link">Mostrar lista</a>
            </div>
            <div class="col-sm-12">
                @yield('content')
            </div>
        </div>
    </div>
</body>
</html>