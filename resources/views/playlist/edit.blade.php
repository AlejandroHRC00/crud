@extends('tema.layout')

@section('title','Editar')

@section('content')
    <h2>
        Editar tarea <i>{{ $song->nombre }}</i>
    </h2>
    <form action="{{ route('playlist.update', $song) }}" method="POST">
        @method('put')
        <x-form :song="$song"/>
    </form>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
