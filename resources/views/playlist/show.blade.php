@extends('tema.layout')

@section('title','Editar')

@section('content')
    <h2>
        <i>{{ $song->nombre }}</i>
    </h2>
    <ul>
        <li>Nombre: <strong>{{ $song->nombre }}</strong></li>
        <li>Artista: <strong>{{ $song->artista }}</strong></li>
        <li>Género: <strong>{{ $song->genero }}</strong></li>
        <li>Año: <strong>{{ $song->ano }}</strong></li>
    </ul>
    <div class="row">
        <div class="col-sm-12 mb-2">
            <form action="{{ route('playlist.destroy', $song) }}" method="POST">
                @csrf
                @method('delete')
                <button class="btn btn-danger btn-sm" type="submit">Borrar</button>
            </form>
        </div>
    </div>
@endsection
