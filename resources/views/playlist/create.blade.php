@extends('tema.layout')

@section('title','Agregar')

@section('content')
    <h1>
        Registrar tarea
    </h1>
    <form action="{{ route('playlist.store') }}" method="POST">
       <x-form/>
    </form>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
