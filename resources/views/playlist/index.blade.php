@extends('tema.layout')

@section('title','Playlist')

@section('content')
    <h3>Playlist</h3>
    <table class="table table-stripped table-hover">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Artista</th>
                <th>Año</th>
                <th>Género</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($songs as $song)
                <tr>
                    <td>
                        {{ $song->nombre }}
                    </td>
                    <td>
                        {{ $song->artista }}
                    </td>
                    <td>
                        {{ $song->genero }}
                    </td>
                    <td>
                        {{ $song->ano }}
                    </td>
                    <td>
                        <a href="{{ route('playlist.edit', $song) }}">Editar</a>
                        <a href="{{ route('playlist.show', $song) }}">Ver</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection
